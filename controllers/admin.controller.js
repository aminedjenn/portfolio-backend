const db = require("../models/index");
const travaux = db.travaux;
const fichiers = db.fichier;
const url = require("url");
const format = url.format;
const formidable = require("formidable")
const fs = require("fs");

exports.getWorks = (req, res) => {
  travaux
    .findAll()
    .then((travaux) => {
      //on récupère ici un tableau "candidature" contenant une liste de candidatures
      res.render("tableau-travaux", { travaux: travaux });
    })
    .catch(function (e) {
      //gestion erreur
      console.log(e);
    });
};

exports.deleteWork = (req, res) => {
  travaux.findByPk(req.params.id).then((travail) => {
    travail.destroy();
  });
  res.redirect("/admin/home");
};

exports.addWork = (req, res) => {
  travaux
    .create({
      titre: req.body.titre,
      description: req.body.description,
      contexte: req.body.contexte,
      linkDescription: req.body.linkDescription,
    })
    .catch(function (e) {
      //gestion erreur
      console.log(e);
    });
  res.redirect("/admin/home");
};

exports.updateWork = (req, res) => {
  travaux
    .update(
      {
        titre: req.body.titre,
        description: req.body.description,
        contexte: req.body.contexte,
        linkDescription: req.body.linkDescription,
      },
      {
        where: {
          id: req.body.id,
        },
      }
    )
    .catch(function (e) {
      //gestion erreur
      console.log(e);
    });
  res.redirect("/admin/home");
};

// fichier

exports.upload = (req, res) => {
  var form = formidable.IncomingForm();
  form.parse(req, (err, fields, files) => {
    fs.rename(
      files.fichier.path,
      "./public/img/portfolio/" + files.fichier.name,
      function (err) {
        if (err) throw err;
      }
    );
    res.redirect(
      format({
        pathname: "/admin/home/fichier",
        query: {
          fileName: files.fichier.name,
        },
      })
    );
  });
};

exports.getFiles = (req, res) => {
  travaux.findAll({ attributes: ["id", "titre"] }).then((travail) => {
    fichiers
      .findAll()
      .then((fichier) => {
        res.render("tableau-fichier", { fichier: fichier, tra: travail });
      })
      .catch(function (e) {
        //gestion erreur
        console.log(e);
      });
  });
};

exports.deleteFile = (req, res) => {
  fichiers.findByPk(req.params.id).then((fichier) => {
    fichier.destroy();
  });
  res.redirect("/admin/home/fichier");
};

exports.addFile = (req, res) => {
  fichiers
    .create({
      nomFichier: req.body.name,
      route: "/img/portfolio/" + req.body.route,
      typeFichier: req.body.type,
      idTravail: req.body.idTravail,
    })
    .catch(function (e) {
      //gestion erreur
      console.log(e);
    });
  res.redirect("/admin/home/fichier");
};

exports.updateFile = (req, res) => {
  fichiers
    .update(
      {
        nomFichier: req.body.name,
        route: req.body.route,
        typeFichier: req.body.type,
        idTravail: req.body.idTravail,
      },
      {
        where: {
          id: req.body.id,
        },
      }
    )
    .catch(function (e) {
      //gestion erreur
      console.log(e);
    });
  res.redirect("/admin/home/fichier");
};
