const db = require("../models/index");
const nodemailer = require("nodemailer");
const travauxDB = db.travaux;
const fichierDB = db.fichier;
require("dotenv").config();


exports.accueil = (req, res) => {

  travauxDB.findAll({ include: [fichierDB] }).then((travaux) => {
    res.render("index", { travauxDB: travaux });
  });
};

exports.contact = (req, res) => {
  res.render("contact");
};

exports.propos = (req, res) => {
  res.render("about");
};

exports.detail = (req, res) => {
  travauxDB
    .findByPk(req.params.id, { include: [fichierDB] })
    .then((travail) => {
      res.render("detail", { travail: travail });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.sendMail = (req, res) => {
  async function main() {
    const transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: "portfoliomail12@gmail.com",
        pass: process.env.NODE_PASS_MAIL,
      },
      tls: {
        rejectUnauthorized: false,
      },
    });
    transporter.sendMail({
      from: req.body.email, // sender address
      to: "portfoliomail12@gmail.com", // recipient addresslist of receivers
      subject: "Mail a partir du Portfolio", // Subject line
      html: `
          <!DOCTYPE html> 
          <html> 
          <head> 
          <meta http-equiv=Content-Type content=text/html; charset=utf-8 /> 
          <title>Un Email Responsive</title 
          <style type=text/css> 
          body {margin: 0; min-width: 100%!important;} 
          .content {width: 100%; max-width: 600px;} 
          </style> 
          </head> 
          <body bgcolor=#f6f8f1> 
          <table width=100% bgcolor=#f6f8f1 style=padding:10% cellpadding=0 cellspacing=0> 
          <tr> 
          <td> 
          <table class=content align=center> 
          <tr> 
          <td> Nom prenom :  
          ${req.body.name}
          ${req.body.surname} 
          </td> 
          <td> Message : 
          ${req.body.message} 
          </td> 
          <td> Mail : 
          ${req.body.email} 
          </td> 
          </tr> 
          </table> 
          </td> 
          </tr> 
          </table> 
          </body> 
          </html>,`,
    });
  }
  
  main().catch(console.error);

  res.redirect("/portfolio/contact");
};
