require("dotenv").config();
const db = require("../models");
const users = db.users;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const url = require("url");


exports.connexion = (req, res) => {
  users
    .findOne({ where: { login: req.body.login } })
    .then((user) => {
      if (!user) {
        return res.render("connexion", {
          message: "Aucun utilisateur avec cette identifiant",
        });
      }
      bcrypt
        .compare(req.body.password, user.password)
        .then((valid) => {
          if (!valid) {
            return res.render("connexion", {
              message: "Mot de passe invalide",
            });
          }
          const token = jwt.sign(
            { userId: user.id },
            process.env.ACCESS_TOKEN_SECRET,
            { expiresIn: "24h" }
          );

          res.cookie("auth", token);
          req.session.user = req.body.login;
          res.redirect(
            url.format({
              pathname: "/portfolio",
              query: { name: req.body.login },
            })
          );
        })
        .catch((error) => {
          res.status(500).json({
            error: new Error(error),
          });
        });
    })
    .catch((error) => {
      res.status(500).json({
        error: error,
      });
    });
}

exports.signup = (req, res) => {
  res.render("connexion");
}