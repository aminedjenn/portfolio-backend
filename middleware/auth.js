const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {


 let token = req.headers.cookie
    .split("; ")
    .filter((token) => {
      return token.split("=")[0] === "auth";
    })[0]

    console.log(token)
  if (token == null) {
    return res.render("req-error.ejs", {
      status: 401,
      title: "Vous n'êtes pas autorisé à acceder à cette page",
    });
  }
  token = token.split("=")[1];

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err) => {
    if (err) {
      return res.render("req-error.ejs", {
        status: 401,
        title: "Vous n'êtes pas autorisé à acceder à cette page",
      });
    }
    next();
  });
};
