/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('archive', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    fromModel: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    originalRecord: {
      type: "LONGTEXT",
      allowNull: true
    },
    originalRecordId: {
      type: "LONGTEXT",
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'archive'
    });
};
