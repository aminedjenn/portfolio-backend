/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('fichier', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    route: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    idTravail: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'travaux',
        key: 'id'
      },
      unique: "fichier_ibfk_1"
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    nomFichier: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    typeFichier: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'fichier'
    });
};
