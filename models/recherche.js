/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('recherche', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    dateC: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    nomEntreprise: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    posteCandidate: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: false
    },
    estContacte: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'recherche'
    });
};
