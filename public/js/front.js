$(function () {
 
  // ------------------------------------------------------ //
  // Main slider
  // ------------------------------------------------------ //

  $("#main-slider").owlCarousel({
    items: 1,
    nav: false,
    dots: true,
    autoplay: true,
    autoplayHoverPause: true,
  });
});

$('[data-toggle="tooltip"]').tooltip()

var animateButton = function (e) {
  e.preventDefault;
  //reset animation
  e.target.classList.remove("animate");

  e.target.classList.add("animate");
  setTimeout(function () {
    e.target.classList.remove("animate");
  }, 700);
};

var bubblyButtons = document.getElementsByClassName("bubbly-button");

for (let i = 0; i < bubblyButtons.length; i++) {
  bubblyButtons[i].addEventListener("click", animateButton, false);
}

var urlcourante = document.location.href;
var urlcourante = urlcourante.replace(/\/$/, "");
queue_url = urlcourante.substring(urlcourante.lastIndexOf("/") + 1);
url = queue_url.toString();
var m = url.split("?");
var element = document.getElementById(m[0]);
if (element) {
  element.className = "sidebar-link active";
}


function update(obj) {
 const o = obj.parentNode.parentNode.parentNode; // recuperation de la ligne tr

  for (let index = 0; index < o.cells.length - 1; index++) {
    const value = o.cells[index].textContent.trim();
    const name = o.cells[index].id;
    o.cells[index].innerHTML =`<input class="form-control" size="5" value="${value}" name="${name}" type="text" placeholder="${name}" required>`;
  }

  obj.style.display = "none";

  const button = document.createElement("button"); // creation des elements
  const input = document.createElement("input");
  const sp2 = obj;
  const parentDiv = sp2.parentNode;

  const valueId = obj.id.split("-")["1"]; // creation du champ caché pour l'id
  parentDiv.insertBefore(input, sp2);
  input.value = valueId;
  input.name = "id";
  input.type = "hidden";

  parentDiv.insertBefore(button, sp2);
  button.className = "btn btn-success";
  button.innerHTML = "Valider";
  button.type = "submit";

  document.getElementsByName("btn-update").forEach((btn) => {
    // passage en disabled de tous les autres boutons
    btn.disabled = true;
  });
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  document.location.reload();
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

{
  const loader = document.querySelector(".preloader")
  window.addEventListener("load", () => {
    loader.classList.add("hide")
  })

}



