const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

// travaux

module.exports = (app) => {
  const adminRouter = require("../controllers/admin.controller");
  app.use("/admin", jsonParser, router);

  const auth = require("../middleware/auth.js");

  router.get("/home", auth, adminRouter.getWorks);
  router.get("/travaux/delete/:id", auth, adminRouter.deleteWork);
  router.post("/travaux/add", auth, adminRouter.addWork);
  router.post("/travaux/update", auth, adminRouter.updateWork);
  router.post("/upload", auth, adminRouter.upload);
  router.get("/home/fichier", auth, adminRouter.getFiles);
  router.get("/fichier/delete/:id", auth, adminRouter.deleteFile);
  router.post("/fichier/add", auth, adminRouter.addFile);
  router.post("/fichier/update", auth, adminRouter.updateFile);
};
