const express = require("express");
const router = express.Router();
const db = require("../models");
const recherche = db.recherche;

/* GET home page. */
router.get("/", function (req, res) {
  res.redirect("/portfolio");
});


module.exports = router;
