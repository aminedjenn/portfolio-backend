const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

module.exports = (app) => {
  const portfolio = require("../controllers/portfolio.controller.js");

  app.use("/portfolio", jsonParser, router);

  router.get("/", portfolio.accueil);
  router.get("/contact", portfolio.contact);
  router.get("/a-propos", portfolio.propos);
  router.get("/detail/:id", portfolio.detail);
  router.post("/contact/send", portfolio.sendMail);
};
