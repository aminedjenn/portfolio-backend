var express = require('express');
const { render } = require('../app');
var router = express.Router();
const db = require('../models/index');
const recherche = db.recherche;


/* GET listing. */


    router.get("/delete/:id", (req,res)=> {
        recherche.findByPk(req.params.id).then(candidature => {
            candidature.destroy();
        })
        res.redirect("/recherche")
    });

    router.post("/add", (req,res) => {
        var date = new Date();
        recherche.create(
            {dateC : date, nomEntreprise : req.body.nom, posteCandidate : req.body.poste}
            ).catch(function (e) {
                    //gestion erreur
                    console.log(e);
                });
        res.redirect("/recherche");
    });

    router.post("/update", (req,res) => {
        
        if(req.body.estContacte === undefined){
            req.body.estContacte = 0;
        }
        console.log(req.body.estContacte);
        recherche.update({ nomEntreprise : req.body.nom, posteCandidate : req.body.poste, estContacte : req.body.estContacte}, { 
               where : {
                    id : req.body.id
                } 
            }
            ).catch(function (e) {
                    //gestion erreur
                    console.log(e);
                });
        res.redirect("/recherche"); 
    });


module.exports = router;
