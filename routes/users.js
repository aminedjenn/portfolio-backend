const express = require("express");
require("dotenv").config();
const router = express.Router();
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json()


module.exports = (app) => {
  const userRouter = require("../controllers/user.controller")
  app.use("/users", jsonParser, router)

  router.post("/connexion", userRouter.connexion)
  router.get("/signup", userRouter.signup)
}


